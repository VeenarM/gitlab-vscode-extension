const vscode = require('vscode');
const execa = require('execa');
const url = require('url');

const currentInstanceUrl = () => vscode.workspace.getConfiguration('gitlab').instanceUrl;

async function fetch(cmd, workspaceFolder) {
  const [git, ...args] = cmd.split(' ');
  let currentWorkspaceFolder = workspaceFolder;

  if (currentWorkspaceFolder == null) {
    currentWorkspaceFolder = '';
  }
  let output = null;
  try {
    output = await execa.stdout(git, args, {
      cwd: currentWorkspaceFolder,
    });
  } catch (ex) {
    // Fail siletly
  }

  return output;
}

async function fetchBranchName(workspaceFolder) {
  const cmd = 'git rev-parse --abbrev-ref HEAD';
  const output = await fetch(cmd, workspaceFolder);

  return output;
}

/**
 * Fetches remote tracking branch name of current branch.
 * This should be used in link openers.
 *
 * Fixes #1 where local branch name is renamed and doesn't exists on remote but
 * local branch still tracks another branch on remote.
 */
async function fetchTrackingBranchName(workspaceFolder) {
  const branchName = await fetchBranchName(workspaceFolder);

  try {
    const cmd = `git config --get branch.${branchName}.merge`;
    const ref = await fetch(cmd, workspaceFolder);

    if (ref) {
      return ref.replace('refs/heads/', '');
    }
  } catch (e) {
    console.log(
      `Couldn't find tracking branch. Extension will fallback to branch name ${branchName}`,
    );
  }

  return branchName;
}

async function fetchLastCommitId(workspaceFolder) {
  const cmd = 'git log --format=%H -n 1';
  const output = await fetch(cmd, workspaceFolder);

  return output;
}

const getInstancePath = () => {
  const pathname = url.parse(currentInstanceUrl()).pathname;
  if (pathname !== '/') {
    // Remove trailing slash if exists
    return pathname.replace(/\/$/, '');
  }

  // Do not return extra slash if no extra path in instance url
  return '';
};

const escapeForRegExp = str => {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
};

const parseGitRemote = remote => {
  if (remote.startsWith('git@')) {
    remote = 'ssh://' + remote;
  }

  const { protocol, host, pathname } = url.parse(remote);

  if (!host || !pathname) {
    return null;
  }

  const pathRegExp = escapeForRegExp(getInstancePath());
  const match = pathname.match(pathRegExp + '/:?(.+)/(.*?)(?:.git)?$');
  if (!match) {
    return null;
  }

  return [protocol, host, ...match.slice(1, 3)];
};

async function fetchRemoteUrl(name, workspaceFolder) {
  let remoteUrl = null;
  let remoteName = name;

  try {
    const branchName = await fetchBranchName(workspaceFolder);
    if (!remoteName) {
      remoteName = await fetch(`git config --get branch.${branchName}.remote`, workspaceFolder);
    }
    remoteUrl = await fetch(`git ls-remote --get-url ${remoteName}`, workspaceFolder);
  } catch (err) {
    try {
      remoteUrl = await fetch('git ls-remote --get-url', workspaceFolder);
    } catch (e) {
      const remote = await fetch('git remote', workspaceFolder);

      remoteUrl = await fetch(`git ls-remote --get-url ${remote}`, workspaceFolder);
    }
  }

  if (remoteUrl) {
    const [schema, host, namespace, project] = parseGitRemote(remoteUrl);

    return { schema, host, namespace, project };
  }

  return null;
}

async function fetchGitRemote(workspaceFolder) {
  const { remoteName } = vscode.workspace.getConfiguration('gitlab');

  return await fetchRemoteUrl(remoteName, workspaceFolder);
}

async function fetchGitRemotePipeline(workspaceFolder) {
  const { pipelineGitRemoteName } = vscode.workspace.getConfiguration('gitlab');

  return await fetchRemoteUrl(pipelineGitRemoteName, workspaceFolder);
}

exports.fetchBranchName = fetchBranchName;
exports.fetchTrackingBranchName = fetchTrackingBranchName;
exports.fetchLastCommitId = fetchLastCommitId;
exports.fetchGitRemote = fetchGitRemote;
exports.fetchGitRemotePipeline = fetchGitRemotePipeline;
